<!-- Footer -->
<footer id="footer" class="text-center footerBackground text-black">
    <div class="navBorderRounded">
        <!-- Grid container -->
        <div class="container p-4">
            <!-- Section: Social media -->
            <section class="mb-4">

                <!-- Instagram -->
                <a class="btn btn-outline-dark btn-floating m-1" href="https://www.instagram.com/buurgersonbike/"
                   target="_blank" role="button"
                ><i class="fab fa-instagram"></i
                    ></a>

                <!-- Github -->
                <a class="btn btn-outline-dark btn-floating m-1" href="https://github.com/EnHamborgerJung"
                   target="_blank" role="button"
                ><i class="fab fa-github"></i
                    ></a>

                <a class="btn btn-outline-dark btn-floating m-1" href="https://gitlab.com/EnHamborgerJung"
                   target="_blank" role="button"
                ><i class="fa-brands fa-gitlab"></i
                    ></a>

                <a class="btn btn-outline-dark btn-floating m-1" href="https://discord.gg/q45rQ34nnH" target="_blank"
                   role="button"
                ><i class="fa-brands fa-discord"></i
                    ></a>

                <a class="btn btn-outline-dark btn-floating m-1" href="https://www.strava.com/athletes/42923386"
                   target="_blank" role="button"
                ><i class="fa-brands fa-strava"></i
                    ></a>

            </section>
            <!-- Section: Social media -->


            <!-- Section: Links -->
            <section>
                <!--Grid row-->
                <div class="d-flex justify-content-around">
                    <!--Grid column-->
                    <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                        <h5 class="text-uppercase">Kontakt</h5>

                        <ul class="list-unstyled mb-0">
                            <li>
                                <a>Nils Dodenhoff</a>
                            </li>
                            <li>
                                <a>Brödermannsweg 77b</a>
                            </li>
                            <li>
                                <a>22453 Hamburg</a>
                            </li>
                            <li>
                                <a>nils.dodenhoff@gmail.com</a>
                            </li>
                            <li>
                                <a>+49 1516 1872385</a>
                            </li>
                        </ul>
                    </div>
                    <!--Grid column-->
                </div>
                <!--Grid row-->
            </section>
            <!-- Section: Links -->
        </div>
        <!-- Grid container -->

        <!-- Copyright -->
        <div class="text-center p-1">
            © 2022 Copyright:
            <a>Nils Dodenhoff</a>
        </div>
        <!-- Copyright -->
    </div>
</footer>
<!-- Footer -->


<script src="{{asset('/js/app.js')}}" type="text/javascript"></script>
<script src="https://kit.fontawesome.com/b99e675b6e.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js"
        integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js"
        integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa"
        crossorigin="anonymous"></script>
</body>
</html>
