<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Nils Dodenhoff</title>

    <link href="{{asset('/css/app.css')}}" rel="stylesheet">
    <link href="{{asset(('/css/app2.css'))}}" rel="stylesheet">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,300,600,800,900" rel="stylesheet" type="text/css">
</head>
<body class="{{(Route::currentRouteName())}}">

<div class="headerBackground" style="width: 100%;
             position: sticky ;
             top: 0 ;">
    <div class="navBorderRounded">
        <nav class="navbar navbar-light navbar-expand-lg">
            <div class="container">
                <a class="navbar-brand" href="{{route('home')}}">Nils Dodenhoff</a>
                <ul class="navbar-nav">


                </ul>
            </div>
        </nav>
    </div>
</div>
