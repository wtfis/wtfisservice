# WTFisService


## LF6 task 

### Schwerpunkt 1: 

- 
-

### Schwerpunkt 2:

- 

### Schwerpunkt 3:

- 


## Chatbot

The bot we want to use 
[Botman](https://botman.io/1.5/installation-laravel)

Tutorial:
(https://www.nicesnippets.com/blog/laravel-botman-chatbot-tutorial-example?utm_content=cmp-true)
we are in step 3

Things i did:
ddev composer require botman/botman
ddev composer require botman/driver-web

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***


## Suggestions for a good README


## Name


## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Installation
git clone https://gitlab.com/wtfis/wtfisservice.git

ddev composer install

## Usage
Start Docker
For starting the Project "ddev start" in the Projectfolder

For stoping "ddev stop"

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Authors and acknowledgment
Lucas S. and Nils D.
